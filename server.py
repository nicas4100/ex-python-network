#!/usr/bin/env python3
# TCP server. Will listen and receive data until client closes connection
# Adapted by Per dahlstrøm
import socket       # Fetch the socket module

#HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
#PORT = 65432        # Port to listen on (non-privileged ports are > 1023)
DataCommingIn = True
#hello
def init_server_socket( host, port ):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((host, port))
    s.listen()
    return s

def close_socket( open_socket ):
    open_socket.close()

def close_connection( conn ):
    conn.close()

def receive_data( conn ):
    data = connection.recv(16)
    if not data: # no more data?
        is_done_receiving = True
    else:
        is_done_receiving = False

    return data, is_done_receiving

def wait_for_connection( open_socket ):
    conn, srcAddress = open_socket.accept() # Wait and create connection object
    return conn, srcAddress

if __name__ == "__main__":
    listen_address = '0.0.0.0'
    port = 65432

    print( "Opening listening socket")
    sock = init_server_socket(listen_address, port )

    print('Awaiting connection on IP: ',sock.getsockname()[0],\
                              ' Port: ',sock.getsockname()[1])
    connection, fromAddress = wait_for_connection( sock )

    print('Connection from:', fromAddress)
    is_done_receiving = False
    while not is_done_receiving:
        receivedData, is_done_receiving = receive_data( connection )
        print(receivedData.decode('utf-8'))

    print( "Closing and cleaning up")
    close_connection( connection )
    close_socket(sock)
